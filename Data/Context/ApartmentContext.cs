﻿using Data.EntityConfig;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Context
{
    public class ApartmentContext : DbContext
    {
        public ApartmentContext() : base("ApartmentDataBase") {}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new ApartmentConfiguration());
            modelBuilder.Configurations.Add(new DwellerConfiguration());
        }
    }
}
