﻿namespace Data.Repository
{
    using Data.Context;
    using Domain.Interfaces.Repositories;
    using System.Collections.Generic;
    using System.Linq;

    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private readonly ApartmentContext _context;

        public RepositoryBase(ApartmentContext context)
        {
            _context = context;
        }

        public void Add(T obj)
        {
            _context.Set<T>().Add(obj);
        }

        public void Remove(T obj)
        {
            _context.Set<T>().Remove(obj);
        }

        public void Update(T obj)
        {
            _context.Entry(obj);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public IList<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }
    }
}
