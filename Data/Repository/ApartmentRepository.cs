﻿namespace Data.Repository
{
    using Data.Context;
    using Domain.Entities;

    public class ApartmentRepository : RepositoryBase<Apartment>
    {
        public ApartmentRepository(ApartmentContext context)
            :base(context)
        {

        }
    }
}
