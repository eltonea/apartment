﻿namespace Data.EntityConfig
{
    using System.Data.Entity;
    using System.Data.Entity.SqlServer;

    public class ApartmentContextConfiguration : DbConfiguration
    {
        public ApartmentContextConfiguration()
        {
            SetProviderServices(SqlProviderServices.ProviderInvariantName, SqlProviderServices.Instance);
        }
    }
}
