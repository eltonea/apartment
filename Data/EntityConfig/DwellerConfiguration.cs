﻿
namespace Data.EntityConfig
{
    using Domain.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class DwellerConfiguration : EntityTypeConfiguration<Dweller>
    {
        public DwellerConfiguration()
        {
            HasKey(a => a.Id);

            Property(a => a.ApartmentId)
                .HasColumnName("ApartmentId");

        }
    }
}
