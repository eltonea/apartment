﻿
namespace Data.EntityConfig
{
    using Domain.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class ApartmentConfiguration : EntityTypeConfiguration<Apartment>
    {
        public ApartmentConfiguration()
        {
            HasKey(c => c.Id);

            Property(c => c.Adress.City)
                .HasColumnName("City");
            Property(c => c.Adress.District)
                .HasColumnName("District");
            Property(c => c.Adress.Floor)
                .HasColumnName("Floor");
            Property(c => c.Adress.Number)
                .HasColumnName("Number");
            Property(c => c.Adress.State)
                .HasColumnName("State");
            Property(c => c.Adress.Street)
                .HasColumnName("Street");

            HasOptional(a => a.Dwellers)
                .WithMany();
        }
    }
}
