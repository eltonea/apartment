﻿namespace Domain.Services
{
    using Domain.Interfaces.Repositories;
    using Domain.Interfaces.Services;
    using System.Collections.Generic;

    public class ServiceBase<T> : IServiceBase<T> where T : class
    {
        private readonly IRepositoryBase<T> _repoBase;

        public ServiceBase(IRepositoryBase<T> repoBase)
        {
            _repoBase = repoBase;
        }

        public void Add(T obj)
        {
            _repoBase.Add(obj);
        }

        public void Remove(T obj)
        {
            _repoBase.Remove(obj);
        }

        public void Update(T obj)
        {
            _repoBase.Update(obj);
        }

        public void SaveChanges()
        {
            _repoBase.SaveChanges();
        }

        public T GetById(int id)
        {
            return _repoBase.GetById(id);
        }

        public IList<T> GetAll()
        {
            return _repoBase.GetAll();
        }
    }
}
