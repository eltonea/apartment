﻿
namespace Domain.Services
{
    using Domain.Entities;
    using Domain.Interfaces.Repositories;
    using Domain.Interfaces.Services;

    public class ApartmentService : ServiceBase<Apartment>
    {
        public ApartmentService(IRepositoryBase<Apartment> repoBase) : base(repoBase){}
    }
}
