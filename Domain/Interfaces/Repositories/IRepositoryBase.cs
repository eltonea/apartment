﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Repositories
{
    public interface IRepositoryBase<T> where T : class
    {
        void Add(T obj);
        void Remove(T obj);
        void Update(T obj);
        void SaveChanges();
        T GetById(int id);
        IList<T> GetAll();
    }
}
