﻿namespace Domain.Interfaces.Repositories
{
    using Domain.Entities;

    public interface IApartmentRepository : IRepositoryBase<Apartment>
    {
    }
}
