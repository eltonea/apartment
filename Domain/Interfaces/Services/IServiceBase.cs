﻿namespace Domain.Interfaces.Services
{
    using System.Collections.Generic;

    public interface IServiceBase<T> where T : class
    {
        void Add(T obj);
        void Remove(T obj);
        void Update(T obj);
        void SaveChanges();
        T GetById(int id);
        IList<T> GetAll();
    }
}
