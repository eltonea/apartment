﻿
namespace Domain.Interfaces.Services
{
    using Domain.Entities;

    public interface IApartmentService : IServiceBase<Apartment>
    {
    }
}
