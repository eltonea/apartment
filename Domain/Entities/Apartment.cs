﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Entities
{
    public class Apartment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Adress Adress { get; set; }
        public IList<Dweller> Dwellers { get; set; }
        public string Observation { get; set; }
    }
}
