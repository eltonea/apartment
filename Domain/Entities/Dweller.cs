﻿namespace Domain.Entities
{
    using System.Collections.Generic;

    public class Dweller
    {
        public int Id { get; set; }
        public int ApartmentId { get; set; }
        public IList<string> NameOfDwellers { get; set; }
        public Apartment Apartment { get; set; }
        public string Telephone { get; set; }
        public string MobilePhone { get; set; }
    }
}
