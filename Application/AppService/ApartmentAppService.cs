﻿namespace Application.AppService
{
    using Application.Interfaces;
    using Domain.Entities;
    using Domain.Interfaces.Services;

    public class ApartmentAppService : AppServiceBase<Apartment>, IApartmentAppService
    {
        public ApartmentAppService(IServiceBase<Apartment> serviceBase) : base(serviceBase){}
    }
}
