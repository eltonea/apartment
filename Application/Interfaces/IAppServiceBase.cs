﻿namespace Application.Interfaces
{
    using System.Collections.Generic;

    public interface IAppServiceBase<T> where T : class
    {
        void Add(T obj);
        void Remove(int id);
        void Update(T obj);
        T GetById(int id);
        IList<T> GetAll();
    }
}
