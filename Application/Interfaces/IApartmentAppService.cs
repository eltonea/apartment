﻿namespace Application.Interfaces
{
    using Domain.Entities;
    public interface IApartmentAppService : IAppServiceBase<Apartment>
    {
    }
}
