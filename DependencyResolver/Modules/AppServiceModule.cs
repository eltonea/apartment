﻿namespace DependencyResolver.Modules
{
    using Application.AppService;
    using Application.Interfaces;

    public class AppServiceModule : BaseResolver
    {
        public override void Load()
        {
            Bind(typeof(IAppServiceBase<>)).To(typeof(AppServiceBase<>));
            Bind(typeof(IApartmentAppService)).To(typeof(ApartmentAppService));
        }
    }
}
