﻿using Domain.Interfaces.Services;
using Domain.Services;
namespace DependencyResolver.Modules
{
    public class ServiceModule : BaseResolver
    {
        public override void Load()
        {
            Bind(typeof(IServiceBase<>)).To(typeof(ServiceBase<>));
            Bind(typeof(IApartmentService)).To(typeof(ApartmentService));
        }
    }
}
