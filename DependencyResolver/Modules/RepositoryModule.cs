﻿
namespace DependencyResolver.Modules
{
    using Data.Context;
    using Data.Repository;
    using Domain.Interfaces.Repositories;

    public class RepositoryModule : BaseResolver
    {
        public override void Load()
        {
            Bind(typeof(ApartmentContext)).ToSelf();
            Bind(typeof(IRepositoryBase<>)).To(typeof(RepositoryBase<>));
            Bind(typeof(IApartmentRepository)).To(typeof(ApartmentRepository));

        }
    }
}
