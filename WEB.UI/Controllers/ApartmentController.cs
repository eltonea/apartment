﻿namespace WEB.UI.Controllers
{
    using Application.Interfaces;
    using Domain.Entities;
    using System.Web.Mvc;

    public class ApartmentController : Controller
    {
        private readonly IApartmentAppService _apartmentAppService;
        public ApartmentController(IApartmentAppService apartmentAppService)
        {
            _apartmentAppService = apartmentAppService;
        }

        public ActionResult Index()
        {
            return View(_apartmentAppService.GetAll());
        }

        public ActionResult Details(int id)
        {
            return View(_apartmentAppService.GetById(id));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Apartment model)
        {
            try
            {
                _apartmentAppService.Add(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            return View(_apartmentAppService.GetById(id));
        }

        //
        // POST: /Apartment/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, Apartment model)
        {
            try
            {
                _apartmentAppService.Update(model);

                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            return View();
        }


        [HttpPost]
        public ActionResult Delete(int id, Apartment model)
        {
            try
            {
                _apartmentAppService.Remove(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }
    }
}
